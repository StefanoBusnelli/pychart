PyChart
=======

Libreria python per generare semplici grafici tramite la libreria Pillow.

Versione 1.1.0

Installazione
-------------

pip install PyChart-busnellistefano -U

Utilizzo
--------

$ python

from pychart import *
from pychart import Mapper
from pychart import Graph
from pychart import Panel 
from pychart import Chart

Classi
------

* Graph

Definisce un grafico che viene disegnato tramite i valori contenuti nell' array values ed la funzione cb_draw.
I parametri passati a Chart sono:
  L'array di valori da disegnare
  La funzione call back cb_draw con cui disegnare ciascun valore
  Il colore con cui disegnare

cb_draw è una funzione call_back che viene richiamata per ciascun valore da disegnare sul grafico.
I parametri passati a cb_draw sono: 
  l'oggetto Image.Draw su cui disegnare
  Mapper per eseguire le trasformazioni lineari tra i valori da rappresentare e le coordinate sull'immagine
  L'indice del valore da disegnare
  L'array contenente tutti i valori
  Il colore con cui disegnare

Es:

def draw_lines( draw, mapper, i, values, color ):
    if i > 0:
        p0 = mapper.map_v_c( ( i - 1, values[ i - 1 ] ) )
        p1 = mapper.map_v_c( ( i    , values[ i     ] ) )
        draw.line( [ p0, p1 ], color, 2 )
v = [ 1.0, 2.0, 3.0, 4.0 ]
c = ( 255, 153, 0, 255 )
G = Graph( v, draw_lines, c )

L'immagine del grafico viene restituita tramite il metodo render di Graph ed è l'oggetto creato con il metodo Image.new di Pillow
I parametri passati al metodo render sono:
  Larghezza dell'immagine
  Altezza dell'immagine
  Bordo

Es:
i = G.render( 800, 600, 10 )

L'immagine poi può essere incollata su altre immagini create con Pillow o salvata.
Es:
u = Image.new( .... )
u.paste( i, ( x, y ) )

i.save( './img-i.png', 'PNG' )
u.save( './img-u.png', 'PNG' )

* Panel

* Chart

* Mapper
