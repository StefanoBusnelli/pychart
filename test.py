from pychart import *

#def test():
#    g = Graph( [ -1.0, 0, 1.0 ], None )
#    img = g.render( 800, 600, 10 )
#    img.save( 'img.png', 'PNG' )

def draw_lines( draw, mapper, i, values, color ):
    if i > 0:
        p0 = mapper.map_v_c( ( i - 1, values[ i - 1 ] ) )
        p1 = mapper.map_v_c( ( i    , values[ i     ] ) )
        draw.line( [ p0, p1 ], color, 2 )

def draw_bars( draw, mapper, i, values, color ):
    pv = mapper.map_v_c( ( i    , values[ i ] ) )
    pz = mapper.map_v_c( ( i - 1, 0 ) )
    w = ( pz[ 0 ] - pv[ 0 ] ) / 2
    
    if ( pv[ 0 ] - w < pv[ 0 ] + w ):
        px0 = pv[ 0 ] - w 
        px1 = pv[ 0 ] + w
    else:
        px0 = pv[ 0 ] + w 
        px1 = pv[ 0 ] - w
    if ( pz[ 1 ] - 1 < pz[ 1 ] + 1):
        py0 = pz[ 1 ] - 1
        py1 = pz[ 1 ] + 1
    else:
        py0 = pz[ 1 ] + 1
        py1 = pz[ 1 ] - 1

    p0 = ( px0, py0 )
    p1 = ( px1, py1 )
    draw.rectangle( [ p0, p1 ], color, 2 )

def test():
    #c = Chart( [ Panel( [ Graph( [ -1.0, 1.0, 2.0, 3.0 ] ) ] ) ] )
    #c = Chart( [ \
    #        Panel( [ \
    #            Graph( \
    #                [-1.0,  0.0, 0.5, 1.0, 1.2, 1.5], \
    #                draw_bars, \
    #                (255, 153, 0, 255) \
    #            ) \
    #        ] ), \
    #        Panel( [ \
    #            Graph(  
    #                [-2.0, 2.0], \
    #                draw_lines, \
    #                (51, 153, 255, 255) \
    #            ) \
    #        ] ) \
    #    ] )
    c = Chart( [ \
            Panel( [ \
                Graph( \
                    [-1.0,  0.0, 0.5, 1.0, 1.2, 1.5], \
                    draw_bars, \
                    (255, 153, 0, 255) \
                ), \
                Graph(  
                    [-2.0, 2.0], \
                    draw_lines, \
                    (51, 153, 255, 255) \
                ) \
            ] ) \
        ] )

    i = c.render( 1280, 900,  10 )
    i.save( './img/img.png', 'PNG' )

if __name__ == '__main__':
    test()
